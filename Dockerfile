FROM maven:3.5.3-jdk-8-slim
MAINTAINER Bruce Taylor <bruce.a.taylor88@gmail.com>

RUN apt-get update && apt-get install -my wget gnupg
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

RUN mkdir /app

WORKDIR /app

VOLUME ~/.m2:/root/.m2


ADD pom.xml /app

RUN mvn clean verify -DskipTests

ADD . /app

RUN mvn package -DskipTests

RUN npm install --prefix src/main/resources/client

RUN npm run build --prefix src/main/resources/client

EXPOSE 8080:8080

CMD ["sh","start.sh"]