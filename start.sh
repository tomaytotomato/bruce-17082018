#!/bin/bash

echo "Starting Service"

echo "Checking for DB migrations"

java -jar target/service.jar db migrate config.yml

echo "Starting service"

java -jar target/service.jar server config.yml