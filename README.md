# Uploadr


Kraken Frontend Assignment

Bruce Taylor -
15/08/2018


## Caveats

Ran out of time doing this project, so there will be bugs and weird issues. The frontend will need to be run in development
mode if running locally as there is no process to build the source files.

No database dependencies need to be installed.

Maven is required if building locally, docker can be used also

## Requirements

### File upload - not implemented on frontend

As a user I would like to upload documents to the service. 
So myself or others can view them at a later time.

### File list - implemented

As a user I would like to view a list of previously uploaded files

### File deletion - not implemented on frontend

As a user I would like to delete a specific file from the web service.


## Other

- Dockerfile
- Git
- Swagger API docs
- Unit tests for backend service
- H2 database


## Questions

* Can users see all files or only the files they have uploaded?

    * If we create an anonymous id which the user will have to remember, to 
    give some file privacy.

* What happens if a file with the same name is uploaded?

    * If we restrict by user, this shouldn't be an issue

* What happens if two users try to delete the same file at the same time?

    * As mentioned before, this type of user collision can be mitigated with some
    sort of anonymous identifier

* Unsafe files (.sh, .so, .php etc.) are filtered out?

    * Validation by the service to prevent unsafe files

* Crazy sized files > 100mb?

    * Need to limit file size

* Are files stored on disk or as BLOBS in a database?

    * Files and their meta data will be stored as one record. 
    Which is easier to scale and reduces the risk of losing data
    or breaking links between a DB record and a physical file.


## Non functional requirements

### Security

All users will be anonymous and will have no state on the web service.

File encryption should be used if not technically challenging by the service or 
impacting performance. - not implemented

All endpoints should use HTTPS - not implemented

No error messages / stack traces will be exposed

Request validation etc.

CSRF attack - not applicable

XSS attack - not applicable

SQL Injection - mitigated by Hibernate and ORM


## Solution Architecture

### Service (API)

The backend service will be built using Java Dropwizard. Which in turn will use
a database to persist uploaded files on behalf of users.
The API will implement standard REST endpoints e.g.

POST `api/files/`

GET `api/files/2149049/`

DELETE `api/files/258922`

For errors appropriate HTTP response codes will be used.

### Application (Webpage)

The frontend will be built using React to browse files.

## Data

For this exercise a H2 in memory database will be used due to time constraints.
H2 allows the database to be effectively embedded within the service, meaning 
there is no dependency concerns of having an externally run DB instance such as MySQL
It also has features like encryption and full text search as standard.


How to start the Uploadr application
---

1. Run `mvn clean install` to build your application
1. Run migrations `java -jar target/applications.jar db migrate`
1. Start application with `java -jar target/application.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

The front end should display

Swagger API docks are at `http://localhost:8080/api/swagger`

Docker way
---

1. Run `docker build . -t uploadr:local`
1. Run `docker run -d uploadr:local`
