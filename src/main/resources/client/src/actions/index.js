export const UPLOAD_FILE = 'UPLOAD_FILE';
export const REQUEST_FILES = 'REQUEST_FILES';
export const DELETE_FILE = 'DELETE_FILE';
export const RECEIVE_FILES = 'RECEIVE_FILES';

export const deleteFile = ({
    type: DELETE_FILE,
});


export const requestFiles  = ({
    type: REQUEST_FILES,
});

export const receiveFiles = (json) => ({
    type: RECEIVE_FILES,
    files: json,
    receivedAt: Date.now()
});


export const deleteFileById = (dispatch, fileId) => {
    dispatch(deleteFile);
    return fetch(`/api/files/${fileId}`, {method : 'delete'})
        .then(dispatch(fetchFiles(dispatch)))
};

export const fetchFiles = (dispatch) => {
    dispatch(requestFiles);
    return fetch('/api/files')
        .then(response => response.json())
        .then(json => dispatch(receiveFiles(json)))
};