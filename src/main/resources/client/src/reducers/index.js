import {combineReducers} from 'redux'
import {
    RECEIVE_FILES,
    REQUEST_FILES
} from '../actions'

const files = (state = {
    isFetching: false,
    items: []
}, action) => {
    switch (action.type) {
        case REQUEST_FILES:
            return {
                ...state,
                isFetching: true,
            };
        case RECEIVE_FILES:
            return {
                ...state,
                isFetching: false,
                items: action.files,
                lastUpdated: action.receivedAt
            };
        default:
            return state
    }
};

const rootReducer = combineReducers({
    files
});

export default rootReducer