import React from 'react'
import PropTypes from 'prop-types'

const Files = ({files, handleClick }) => (
    <ul>
        {files.map((file, i) =>
            <li key={i}>{file.name}
                <button onClick={() => (handleClick(file.id))}>Delete</button>
            </li>
        )}
    </ul>
);

Files.propTypes = {
    files: PropTypes.array.isRequired,
    handleClick: PropTypes.func.isRequired
};

export default Files
