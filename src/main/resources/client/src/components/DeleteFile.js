import React from 'react'
import PropTypes from 'prop-types'

const DeleteFile = ({fileId, onChange}) => (
    <span>
        <button >Delete</button>
  </span>
);

DeleteFile.propTypes = {
    fileId: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export default DeleteFile