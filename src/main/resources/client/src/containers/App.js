import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {fetchFiles, deleteFileById} from '../actions'
import Files from '../components/Files'

class App extends Component {
    static propTypes = {
        files: PropTypes.array.isRequired,
        isFetching: PropTypes.bool.isRequired,
        dispatch: PropTypes.func.isRequired,
        deleteFileById : PropTypes.func,
    };

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(fetchFiles);
    }

    handleClick = fileId => {
        this.props.deleteFileById(fileId);
    };

    render() {
        const {files, isFetching, dispatch} = this.props;
        const isEmpty = files.length === 0;
        return (
            <div>
                <h1>Uploadr</h1>
                {isEmpty
                    ? (isFetching ? <h2>Loading...</h2> : <h2>No files.</h2>)
                    : <div style={{opacity: isFetching ? 0.5 : 1}}>
                        <Files files={files} handleClick={this.handleClick}/>
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {isFetching, items: files} = state.files;

    return {
        files,
        isFetching,
    }
};


export default connect(mapStateToProps)(App)
