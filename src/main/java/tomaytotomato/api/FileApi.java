package tomaytotomato.api;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

/**
 * Defines all contract methods that the API can perform in the domain of Files
 */
public interface FileApi {

    /**
     * FileEntity upload method
     * @param file inputstream
     * @param fileDetail content type
     */
    Response uploadFile(InputStream file, FormDataContentDisposition fileDetail);

    /**
     * Delete file
     *
     * Note: not really safe in current form, as any ID can be guessed and then deleted
     * @param id
     */
    void deleteFile(Integer id);

    /**
     * Retrieve all files
     * @return
     */
    List<FileResponse> allFiles();

}
