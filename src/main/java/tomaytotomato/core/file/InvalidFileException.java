package tomaytotomato.core.file;

/**
 * Used when a file has failed validation
 */
public class InvalidFileException extends Exception {

    public InvalidFileException(final String message) {
        super(message);
    }
}
