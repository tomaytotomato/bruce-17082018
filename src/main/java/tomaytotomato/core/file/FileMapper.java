package tomaytotomato.core.file;

import tomaytotomato.api.FileResponse;
import tomaytotomato.db.FileEntity;

/**
 * Maps internal domain models into external api objects
 */
public class FileMapper {

    public FileResponse map(final FileEntity fileEntity) {

        return FileResponse.builder()
                .setId(fileEntity.getId())
                .setName(fileEntity.getName())
                .setTimestamp(fileEntity.getTimestamp())
                .setType(fileEntity.getType())
                .build();
    }
}
