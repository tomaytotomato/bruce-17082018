package tomaytotomato.core.file;

import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteStreams;
import liquibase.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Utility class that validates files against their attributes
 */
public class FileValidator {

    public static final long MAX_FILE_SIZE_BYTES = 1048576; //10mb
    public static final long MIN_FILE_SIZE_BYTES = 1; // 1 byte and chomp
    public static final List<String> validTypes = ImmutableList.of("png", "jpeg",
            "jpg", "txt", "docx", "pdf", "gif", "svg", "odt");


    public static boolean validSize(final InputStream file) throws IOException {
        final long fileSize = ByteStreams.toByteArray(file).length;
        return fileSize >= MIN_FILE_SIZE_BYTES && fileSize < MAX_FILE_SIZE_BYTES;
    }

    public static boolean validateFileName(final String fileName) {
        return !StringUtils.isEmpty(fileName);
    }

    public static boolean validType(final String type) {
        return validTypes.contains(type);
    }

}
