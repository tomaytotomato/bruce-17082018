package tomaytotomato.core.file;

import tomaytotomato.api.FileResponse;

import java.io.InputStream;
import java.util.List;

/**
 * Manages the persistence of Files within the service
 */
public interface FileService {

    Integer create(String name, String type, InputStream content) throws FileServiceException, InvalidFileException;

    void delete(Integer id);

    List<FileResponse> findAll();

}
