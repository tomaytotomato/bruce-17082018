package tomaytotomato.core.file;

import org.hibernate.engine.jdbc.BlobProxy;
import tomaytotomato.api.FileResponse;
import tomaytotomato.db.FileRepository;
import tomaytotomato.db.FileEntity;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of FileService using FileRepository to persist to a database
 */
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;
    private final FileMapper fileMapper;

    public FileServiceImpl(final FileRepository fileRepository) {
        this.fileRepository = checkNotNull(fileRepository, "FileEntity DAO must be initialised");
        this.fileMapper = new FileMapper();
    }

    @Override
    public Integer create(final String fileName, final String fileType, final InputStream file) throws FileServiceException, InvalidFileException {

        try {

            if(!FileValidator.validSize(file)) {
                throw new InvalidFileException("Invalid file size must be between " + FileValidator.MIN_FILE_SIZE_BYTES
                + " and " + FileValidator.MAX_FILE_SIZE_BYTES);
            }

            if(!FileValidator.validType(fileType)) {
                throw new InvalidFileException("Invalid file type, must be one of the following : "
                        + FileValidator.validTypes.toString());
            }

            if(!FileValidator.validateFileName(fileName)) {
                throw new InvalidFileException("Invalid file name, cannot be blank or use special chars");
            }

            final Optional<FileEntity> existingFile = this.fileRepository.findByFileName(fileName);

            if(!existingFile.isPresent()) {

                final Blob fileBlob = BlobProxy.generateProxy(file, file.available());
                final FileEntity newFileEntity = FileEntity.builder()
                        .setName(fileName)
                        .setType(fileType)
                        .setTimestamp(LocalDateTime.now())
                        .setContent(fileBlob)
                        .build();

                return fileRepository.create(newFileEntity);
            } else {
                return existingFile.get().getId();
            }

        } catch (IOException e) {
            throw new FileServiceException(e);
        }
    }

    @Override
    public void delete(final Integer id) {
        final Optional<FileEntity> fileEntity = this.fileRepository.findById(id);
        fileEntity.ifPresent(this.fileRepository::delete);
    }

    @Override
    public List<FileResponse> findAll() {

        return this.fileRepository.findAll()
                .stream()
                .map(fileMapper::map)
                .collect(Collectors.toList());
    }
}
