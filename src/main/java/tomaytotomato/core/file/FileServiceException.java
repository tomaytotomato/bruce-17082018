package tomaytotomato.core.file;

/**
 * FileService domain exception
 */
public class FileServiceException extends Exception {

    public FileServiceException() {
    }

    public FileServiceException(final String message) {
        super(message);
    }

    public FileServiceException(final Throwable cause) {
        super(cause);
    }
}
