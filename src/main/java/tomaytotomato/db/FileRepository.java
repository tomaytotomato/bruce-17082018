package tomaytotomato.db;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

/**
 * Provides query methods for file records in the database
 */
public class FileRepository extends AbstractDAO<FileEntity> {

    public FileRepository(final SessionFactory factory) {
        super(factory);
    }

    public Integer create(final FileEntity fileEntity) {
        return persist(fileEntity).getId();
    }

    public Optional<FileEntity> findById(final Integer id) {
        return Optional.ofNullable(get(id));
    }

    public Optional<FileEntity> findByFileName(final String fileName) {
        return Optional.ofNullable(
                query("SELECT f FROM FileEntity f WHERE f.name = :name ")
                        .setParameter("name", fileName)
                        .getSingleResult());
    }

    public List<FileEntity> findAll() {
        return list(query("SELECT f FROM FileEntity f"));
    }

    public void delete(final FileEntity fileEntity) {
        currentSession().delete(fileEntity);
    }
}