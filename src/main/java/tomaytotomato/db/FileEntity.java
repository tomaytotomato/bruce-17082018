package tomaytotomato.db;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.sql.Blob;
import java.time.LocalDateTime;

/**
 * Entity class for a FileEntity record
 */
@Entity
@Table(name = "file")
public class FileEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @Column(name = "content")
    private Blob content;

    public FileEntity() {
    }

    public FileEntity(final Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.type = builder.type;
        this.timestamp = builder.timestamp;
        this.content = builder.content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Blob getContent() {
        return content;
    }

    public void setContent(final Blob content) {
        this.content = content;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final FileEntity that = (FileEntity) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(name, that.name)
                .append(type, that.type)
                .append(timestamp, that.timestamp)
                .append(content, that.content)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(type)
                .append(timestamp)
                .append(content)
                .toHashCode();
    }

    public static class Builder {

        private Integer id;
        private String name;
        private String type;
        private LocalDateTime timestamp;
        private Blob content;

        public Builder setId(final Integer id) {
            this.id = id;
            return this;
        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setType(final String type) {
            this.type = type;
            return this;
        }

        public Builder setTimestamp(final LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder setContent(final Blob content) {
            this.content = content;
            return this;
        }

        public FileEntity build() {
            return new FileEntity(this);
        }
    }
}
