package tomaytotomato.resources;

import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import liquibase.util.file.FilenameUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.service.spi.ServiceException;
import tomaytotomato.api.FileApi;
import tomaytotomato.api.FileResponse;
import tomaytotomato.core.file.FileService;
import tomaytotomato.core.file.FileServiceException;
import tomaytotomato.core.file.InvalidFileException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Public HTTP implementation of the FileApi using REST endpoint
 */
@Api
@Path("/files")
public class FileResource implements FileApi {

    private final FileService fileService;

    public FileResource(final FileService fileService) {
        this.fileService = checkNotNull(fileService, "FileEntity service cannot be null");
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    @Override
    public Response uploadFile(@FormDataParam("file") final InputStream file,
                               @FormDataParam("file") final FormDataContentDisposition fileDetail) {

        try {

            final String fileName = fileDetail.getFileName();
            final String fileType = FilenameUtils.getExtension(fileName);



            this.fileService.create(fileName, fileType, file);
            return Response.ok().build();
        } catch (FileServiceException e) {
            throw new ServiceException(e.getLocalizedMessage());
        } catch (InvalidFileException e) {
            throw new BadRequestException(e);
        }
    }

    @DELETE
    @UnitOfWork
    @Override
    public void deleteFile(@QueryParam("id") final Integer id) {
        this.fileService.delete(id);
    }

    @GET
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public List<FileResponse> allFiles() {
        return this.fileService.findAll();
    }
}
