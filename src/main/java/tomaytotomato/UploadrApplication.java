package tomaytotomato;

import io.dropwizard.Application;
import io.dropwizard.bundles.assets.ConfiguredAssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import tomaytotomato.core.file.FileService;
import tomaytotomato.core.file.FileServiceImpl;
import tomaytotomato.db.FileEntity;
import tomaytotomato.db.FileRepository;
import tomaytotomato.health.DatabaseHealthCheck;
import tomaytotomato.resources.FileResource;

/**
 * Entrypoint class to the webservice
 */
public class UploadrApplication extends Application<UploadrConfiguration> {

    private final HibernateBundle<UploadrConfiguration> hibernateBundle =
            new HibernateBundle<UploadrConfiguration>(FileEntity.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(UploadrConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    public static void main(final String[] args) throws Exception {
        new UploadrApplication().run(args);
    }

    @Override
    public String getName() {
        return "Uploadr";
    }

    /**
     * Initialize phase of service lifecycle prepares all dependencies before starting
     * @param bootstrap
     */
    @Override
    public void initialize(final Bootstrap<UploadrConfiguration> bootstrap) {

        //setup swagger api docs page
        bootstrap.addBundle(new SwaggerBundle<UploadrConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(
                    UploadrConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });

        //mutlipart forms library
        bootstrap.addBundle(new MultiPartBundle());

        //configure jetty to serve static content from client folder in resources path
        bootstrap.addBundle(new ConfiguredAssetsBundle("/client/build", "/", "index.html"));

        //db migrations
        bootstrap.addBundle(new MigrationsBundle<UploadrConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(UploadrConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        //hibernate jpa
        bootstrap.addBundle(hibernateBundle);

    }

    /**
     * Start the webservice with all required resource endpoints
     * @param configuration
     * @param environment
     */
    @Override
    public void run(final UploadrConfiguration configuration,
                    final Environment environment) {

        final FileRepository fileRepository = new FileRepository(hibernateBundle.getSessionFactory());
        final FileService fileService = new FileServiceImpl(fileRepository);

        environment.jersey().register(new FileResource(fileService));
        environment.healthChecks().register("database", new DatabaseHealthCheck());

    }
}
