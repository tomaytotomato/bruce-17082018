package tomaytotomato.core.file;

import liquibase.util.file.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import tomaytotomato.db.FileEntity;
import tomaytotomato.db.FileRepository;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class FileServiceImplTest {

    private FileService fileService;
    private FileRepository fileRepository;

    @Before
    public void setup() {
        fileRepository = mock(FileRepository.class);
        fileService = new FileServiceImpl(fileRepository);
    }

    @Test
    public void createFileValid() throws Exception {
        final Integer EXPECTED = 521;
        final File validFile = new File("src/test/resources/safeFile.txt");
        final FileInputStream file = new FileInputStream(validFile);

        when(fileRepository.findByFileName(validFile.getName())).thenReturn(null);
        when(fileRepository.create(any())).thenReturn(EXPECTED);

        assertEquals(fileService.create("foo", "txt", file), EXPECTED);

        verify(fileRepository, times(1)).findByFileName(any());
        verify(fileRepository, times(1)).create(any());
    }

    @Test
    public void createFileAlreadyExists() throws Exception {
        final Integer EXPECTED = 533;

        final FileEntity existing = FileEntity.builder()
                .setName("existingFile.txt")
                .setId(EXPECTED)
                .build();

        final File existingFile = new File("src/test/resources/existingFile.txt");
        final FileInputStream file = new FileInputStream(existingFile);

        when(fileRepository.findByFileName(existingFile.getName())).thenReturn(Optional.ofNullable(existing));

        assertEquals(EXPECTED, fileService.create(existingFile.getName(),
                FilenameUtils.getExtension(existing.getName()),
                file));

        verify(fileRepository, times(1)).findByFileName(any());
        verify(fileRepository, times(0)).create(any());
    }

    @Test(expected = InvalidFileException.class)
    public void createFileInvalidType() throws Exception {
        final File invalidFile = new File("src/test/resources/unsafeFile.sh");
        final FileInputStream file = new FileInputStream(invalidFile);

        fileService.create(invalidFile.getName(),
                FilenameUtils.getExtension(invalidFile.getName()),
                file);

        verify(fileRepository, times(1)).findByFileName(any());
        verify(fileRepository, times(0)).create(any());
    }

    @Test(expected = InvalidFileException.class)
    public void createFileInvalidTooLarge() throws Exception {
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(new byte[20000000]);

        fileService.create("foo.txt", "txt", inputStream);
    }

    @Test(expected = InvalidFileException.class)
    public void createFileInvalidEmpty() throws Exception {
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(new byte[0]);
        fileService.create("small.txt", "txt", inputStream);
    }

    @Test
    public void deleteFileExists() {

        final Integer ID = 3592;
        final FileEntity entityToBeDeleted = FileEntity.builder()
                .setId(ID)
                .setName("Delete ME.png")
                .build();

        when(fileRepository.findById(ID)).thenReturn(Optional.ofNullable(entityToBeDeleted));

        this.fileService.delete(ID);

        verify(fileRepository, times(1)).findById(ID);
        verify(fileRepository, times(1)).delete(entityToBeDeleted);
    }

    @Test
    public void deleteFileDoesntExist() {

        final Integer ID = 125;

        when(fileRepository.findById(ID)).thenReturn(Optional.empty());

        this.fileService.delete(ID);

        verify(fileRepository, times(1)).findById(ID);
        verify(fileRepository, times(0)).delete(any());
    }
}