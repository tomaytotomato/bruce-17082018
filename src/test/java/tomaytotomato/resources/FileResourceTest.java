package tomaytotomato.resources;


import io.dropwizard.testing.junit.ResourceTestRule;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import tomaytotomato.core.file.FileService;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;

public class FileResourceTest {

    private static final FileService fileService = mock(FileService.class);

    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new FileResource(fileService))
            .build();

    public void setUp() {
//        when(fileService.create(any(), any())).thenReturn(1294);
    }

    public void uploadSafeFile() {

        final MultiPart multiPart = new MultiPart()
                .bodyPart(new BodyPart(new File("safeFile.txt"), MediaType.TEXT_PLAIN_TYPE));


        assertEquals(resources.target("/api/files")
                .request()
                .post(Entity.entity(multiPart, multiPart.getMediaType()))
                .getStatus(), Response.Status.OK);

//        verify(fileService).create(any());
    }

    public void deleteFile() {
    }

    /**
     * Reset all mocks after each test
     */
    public void tearDown() {
        reset(fileService);
    }

}